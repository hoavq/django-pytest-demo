# Demo Django-Pytest project

Author: Joe - Vu Quang Hoa

Email: hoavq@codignify.com

Website: https://codignify.com

Presentation: https://drive.google.com/drive/folders/1nCCS4scintDyB0TE8X1TYkU8t-OF9OlA?usp=sharing

This project is a sample Django project with pytest


## 1. Local installation

### 1.1 Pre-requisite packages

```
pyenv
virtualenv
```

### 1.2 Install packages

```sh
pyenv virtualenv 3.10 demo
pyenv activate demo
```

Then install application packages

```sh
pip install -r requirements.txt
pip install -r requirements.dev.txt  # for local only
```


### 1.3 Create `demo/settings/.env`

```sh
cp demo/settings/.env.example demo/settings/.env
```

### 1.4 Create local database name and user

Download the psql to your machine and start it, then create database + user

```sql
DROP DATABASE IF EXISTS demo;

CREATE DATABASE demo;

CREATE ROLE demo WITH LOGIN PASSWORD 'password';
GRANT ALL PRIVILEGES ON DATABASE demo TO demo;
ALTER USER demo SUPERUSER;
```

### 1.5 Migrate database

```
python manage.py migrate
```

### 1.6 Run application

```
python manage.py runserver
```

### 1.7 Run Celery background tasks (including celery-beat)

```
celery -A demo beat -l info
```

## 2. Pre-commit

In the root directory

```sh
pre-commit install
```


## 3. Run tests

```sh
pytest
pytest -m fun  # test fun tests
pytest -m api  # test api tests
pytest -m task  # test task tests
```
