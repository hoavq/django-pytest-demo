import pytest


@pytest.fixture(scope="session")
def json_headers():
    headers = {
        "content_type": "application/json",
    }

    return headers
