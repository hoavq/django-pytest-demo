import pytest


@pytest.mark.usefixtures("class_fixture")
class TestClassScopeFixture1:
    def test_sum(self):
        assert self.year + self.current_age == 2022

    def test_substract(self):
        assert self.year - self.current_age == 1954


@pytest.mark.usefixtures("class_fixture")
class TestClassScopeFixture2:
    def test_sum(self):
        assert self.year + self.current_age == 2022

    def test_substract(self):
        assert self.year - self.current_age == 1954
