from django.db import models

from libs.base_model import BaseModel


class Product(BaseModel):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255, unique=True)
    quantity = models.IntegerField()

    category = models.ForeignKey(
        "Category",
        related_name="products",
        on_delete=models.CASCADE,
    )

    @property
    def is_active(self):
        return self.quantity > 0
