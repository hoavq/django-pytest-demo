import factory

from ..models import Category


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category
        django_get_or_create = ("name",)

    name = factory.Sequence(lambda n: f"Category Name {n}")
    address = factory.Sequence(lambda n: f"Address {n}")
