import factory

from ..models import Product
from .category import CategoryFactory


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product
        django_get_or_create = ("code",)

    category = factory.SubFactory(CategoryFactory)

    name = factory.Sequence(lambda n: f"Product Name {n}")
    code = factory.Sequence(lambda n: f"Code{n}")
    quantity = factory.Faker("pyint", min_value=0, max_value=1000)
